package CA1prep;

public class library {

	public static void main(String[] args) {
		
		Book book1 = new Book ("Lord of the rings", "JRR Tolkien", "Fantasy");
		System.out.println("we have "+Book.howManyBooks()+" in our library");
		System.out.println(book1.toString());
		Book book2 = new Book("Gardens of the moon", "Steven Erikson","Epic Fantasy");
		System.out.println("we have "+Book.howManyBooks()+" in our library");
		System.out.println(book2.toString());
		Book book3 = new Book("Song of Ice and Fire", "George R.R. Martin", "Political Fantasy");
		System.out.println("we have "+Book.howManyBooks()+" in our library");
		System.out.println(book3.toString());
		book1.borrow(book1);
		System.out.println(book1.toString());
	}

}
