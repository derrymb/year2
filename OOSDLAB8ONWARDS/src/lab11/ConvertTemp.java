package lab11;

//Converts a Fahrenheit temperature entered by the user to
//Celsius, or vice versa

import java.awt.*;
import java.awt.event.*;



//Driver class
public class ConvertTemp {
	public static void main(String[] args) {
		Frame frame = new ConvertTempFrame("Temperature Conversion");
		frame.setSize(400, 100);
		frame.setLocation(1300,550);
		frame.setVisible(true);
	}
}
//Frame class
class ConvertTempFrame extends Frame {
	private TextField fahrenField = new TextField();
	private TextField celsiusField = new TextField();
	private double fah =0;
	private double cel =0;
	private String fahOut = "";
	private String celOut = "";

	// Constructor
	public ConvertTempFrame(String title)	{
		// Set title for frame and choose layout
		super(title);
		setLayout(new GridLayout(2, 2));

		// Add Fahrenheit label and text field to frame
		add(new Label("Fahrenheit"));
		add(fahrenField);
		// Attach listener to text field

		// Add Celsius label and text field to frame
		add(new Label("Celsius"));
		add(celsiusField);
		// Attach listener to text field

		// Attach window listener
		addWindowListener(new WindowCloser());
		fahrenField.addActionListener(new FahrenheitListener());
		celsiusField.addActionListener(new CelsiusListener());
	}


	// Listener for window
	class WindowCloser extends WindowAdapter 
	{
		public void windowClosing(WindowEvent evt) 
		{
			System.exit(0);
		}
	}
	//(0°C × 9/5) + 32 = 32°F

	class FahrenheitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent evt) {
			fah = Double.parseDouble(fahrenField.getText());
			cel = (fah - 32) * 5/9;
			celOut = Double.toString(cel);
			celsiusField.setText(celOut);
			
		}
		
	}
	
	// (32°F − 32) × 5/9 = 0°C

	class CelsiusListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent evt) {
			cel = Double.parseDouble(celsiusField.getText());
			fah = cel * 9/5 + 32;
			fahOut = Double.toString(fah);
 			fahrenField.setText(fahOut);
			
		}
		
	}
}