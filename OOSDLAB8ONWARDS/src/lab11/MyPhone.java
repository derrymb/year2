package lab11;

import java.awt.*;
import java.awt.event.*;

// Driver class
public class MyPhone 
{
	public static void main(String[] args) 
	{
	    Frame f = new PhoneFrame("My Phone");
	    f.setSize(250, 300);
	    f.setVisible(true);
	}
}

// Frame class
class PhoneFrame extends Frame 
{
	private TextField display = new TextField(20);
	public PhoneFrame(String title) 
	{
	    super(title);
	    Panel displayPanel = new Panel();
	    displayPanel.add(display); 												// adding the display to the panel
	    add("North", displayPanel);
	    display.setEditable(false); 											// turn this on so you can not type in here
	    
		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new GridLayout(4, 3, 10, 10));
		for (int i = 1; i <= 9; i++)
		buttonPanel.add(new Button(i + ""));
		buttonPanel.add(new Button("*"));
		buttonPanel.add(new Button("0"));
		buttonPanel.add(new Button("#"));
	
		Panel centerPanel = new Panel();
		centerPanel.add(buttonPanel);
		add("Center", centerPanel);
	
		Panel bottomPanel = new Panel();
		bottomPanel.add(new Button("Dial"));
		add("South", bottomPanel);

	    // Attach window listener
	    addWindowListener(new WindowCloser());

	}
}

// Listener for window
class WindowCloser extends WindowAdapter 
{
	public void windowClosing(WindowEvent evt) 
	{
    	System.exit(0);
  	}
}