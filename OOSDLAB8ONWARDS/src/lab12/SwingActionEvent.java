package lab12;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SwingActionEvent extends JFrame implements ActionListener {
	
	JButton b = new JButton("Click Me");
	
	public SwingActionEvent(String title) {
		
		super(title);
			setSize(300,100);
			setLocation(500,400);
			b.addActionListener(this);
			getContentPane().add(b);
			setVisible (true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		b.setBackground(Color.CYAN);
		
	}


	public static void main(String[] args) {
		new SwingActionEvent("Swing Button Test");
	}
}