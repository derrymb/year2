package lab12;

import javax.swing.*;

public class ImageIconExample extends JFrame {
	public ImageIconExample(String title ) {
		super( title ) ;
		JFrame frame = new JFrame("ImageIcon Example");
		ImageIcon icon = new ImageIcon(this.getClass () .getResource("smallfrog.jpg")) ;
		JPanel panel = new JPanel();
		JButton button = new JButton(icon);
		panel .add(button);
		frame.getContentPane().add(panel);
		frame.pack();
		frame. setVisible (true) ;
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String [] args) {
		new ImageIconExample("ImageiconExample");
	}
}