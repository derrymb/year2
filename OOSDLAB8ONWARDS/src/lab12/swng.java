package lab12;

import javax .swing.*;
import java .awt.*;
import java .awt.event.*;
public class swng extends JFrame {
	private static String labelPrefix = "Number of button clicks : ";
	private int numClicks = 0;
	JLabel label = new JLabel( labelPrefix + "0 ");
	
	public swng( String title ) {
		super( title ) ;
		// operation to do when the window is closed .
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		setLocation (500, 400);
		JButton button = new JButton("I'm a Swing button!");
		button.addActionListener(new ActionListener () { //This is known as an anonymous function
			
		public void actionPerformed(ActionEvent e) {
			label . setText( labelPrefix + ++numClicks);
		}
	});
		JPanel panel = new JPanel();
		panel .add(button);
		panel .add(label ) ;
		getContentPane().add(panel);
		pack();
		setVisible (true) ;
	}
	public static void main(String [] args) {
		new swng("SwingApplication");
	}
}