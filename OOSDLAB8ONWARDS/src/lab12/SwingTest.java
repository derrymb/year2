package lab12;

import javax.swing.*;

public class SwingTest {
	public static void main (String[] args) {
		JFrame frame = new JFrame("HelloWorldString");
		frame.setBounds(500, 400, 300, 100);
		final JLabel label = new JLabel("Hello World");
		frame.getContentPane().add(label);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
