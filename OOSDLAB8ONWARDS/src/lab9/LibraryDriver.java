package lab9;

public class LibraryDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LibraryItem[] library = new LibraryItem[10];
		
		Book book1 = new Book("Steven Erikson","Gardens of the moon",750);
		library[0] = book1;
		CD cd1 = new CD("DaftPunk", "Discovery", 14);
		library[1] = cd1;
		System.out.println(library[0].toString());
		System.out.println(library[1].toString());
	}

}
