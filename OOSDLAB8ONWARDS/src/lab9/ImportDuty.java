package lab9;

public interface ImportDuty {

	final public double CARTAXRATE = 0.10;
	final public double HGVTAXRATE = 0.15;
	
	double calculateDuty();
}
