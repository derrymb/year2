package lab8;

public class Rectangle extends TwoDShape {

	private double length;
	private double breath;
	
	public Rectangle(String name, String colour, double length, double breath) {
		super(name, colour);
		setLength(length);
		setBreath(breath);
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getBreath() {
		return breath;
	}

	public void setBreath(double breath) {
		this.breath = breath;
	}
	@Override
	public double area() {
		return length*breath;
	}
	
}
