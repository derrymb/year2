package lab8;

public class Cylinder extends ThreeDShape {

	private double height;
	private double radius;
	public Cylinder(String name, String colour, double height, double radius) {
		super(name, colour);
		setHeight(height);
		setRadius(radius);
	}
	
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double volume() {
		return Math.PI * Math.pow(radius, 2) * height;
	}

	@Override
	public double area() {
		return (2*Math.PI*radius*height)+(Math.PI * Math.pow(radius, 2)); 
	}

}
