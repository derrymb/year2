package lab10;

//Displays a frame containing two buttons. Pressing the
//"Lighter" button lightens the background of the frame.
//Pressing the "Darker" button darkens the background.

import java.awt.*;
import java.awt.event.*;

//Driver class
public class MyColours {
	public static void main(String[] args) {
		Frame f = new ChangeColourFrame2("Change Color");
 	f.setSize(500, 250);
 	f.setVisible(true);
	}
}

//Frame class
class ChangeColourFrame2 extends Frame {
	

	public ChangeColourFrame2(String title) {
	    // Set title, layout, and background color
	    super(title);
	    setLayout(new FlowLayout());
	    setBackground(Color.gray);
	
	    // Create button listener
	    ButtonListener listener = new ButtonListener();
	
	    // Add "Lighter" button to frame and attach listener
	    Button b = new Button("Lighter");
	    add(b);
	    b.addActionListener(listener);
	
	    // Add "Darker" button to frame and attach listener
	    b = new Button("Darker");
	    add(b);
	    b.addActionListener(listener);
	    
	    // Green Button
	    b = new Button("Green");
	    add(b);
	    b.addActionListener(listener);
	    
	    // Blue Button
	    b = new Button("Blue");
	    add(b);
	    b.addActionListener(listener);
	    
	    // Red Button
	    b = new Button("Red");
	    add(b);
	    b.addActionListener(listener);
	    
	    // Attach window listener
	    addWindowListener(new WindowCloser());
	}
	
	// Listener for both buttons
	class ButtonListener implements ActionListener {
 	public void actionPerformed(ActionEvent evt) {
   		Color currentBackground = getBackground();
   		String buttonLabel = evt.getActionCommand();

   		// Test label on button and change background color
			if (buttonLabel.equals("Lighter"))
		    	setBackground(currentBackground.brighter());
		    else if (buttonLabel.equals("Blue"))
		    	setBackground(Color.BLUE);
		    else if (buttonLabel.equals("Green"))
		    	setBackground(Color.GREEN);
		    else if (buttonLabel.equals("Red")) 
		    	setBackground(Color.RED);
		    else if (buttonLabel.equals("Darker"))
		    	setBackground(currentBackground.darker());
 	}
	}

	// Listener for window
	class WindowCloser extends WindowAdapter {
 	public void windowClosing(WindowEvent evt) {
   		System.exit(0);
 	}
	}
}