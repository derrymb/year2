package oosd.inheritance;

public class arrayTest {
	
	public static void main(String[] args)
	  {
	    int num = 42;
	    int[] arr = new int[8];
	    arr[0] = 0;
	    arr[1] = 1;
	    arr[2] = 2;
	    arr[3] = 3;
	    arr[4] = 4;
	    int end = 5;
	    for(int i = end; i > 0; i--)
	      arr[i] = arr[i-1];
	    arr[0] = num;
	    end++;
	    for (int j = 0; j < arr.length; j++)
	    System.out.println(arr[j]);
	   System.out.println(end);
	   
	   int[][] arr1 = new int[][] {	{1,2,3,4,5},
	                           		{1,2,3,4,5},
	                           		{1,2,3,4,5},
	                           		{1,2,3,4,5},
	                           		{1,2,3,4,5}};
	   int sum1 = 0;
	   for (int i = 0; i < 4; i++) {
		   for (int j = i+1; j < 5; j++) {
			   sum1 += arr1[i][j];
			   System.out.println("("+i + ", "+ j +")");
		   }
	   }
	   System.out.println("the sum of the array over the diagonal is: " + sum1);
	   int sum = 0;
	   for (int row = 1; row < 5; row++) {
		   for (int col = 0; col < row; col++) {
			   sum += arr1[row][col];
			   System.out.println("("+row + ", "+ col +")");
		   }
	   }
		   System.out.println("the sum of the array under the diagonal is: " + sum);
	   
	}

}
