package scetches;

public class Office {
	private int office;
	private int noOfEmployees = 0;
	private static int officeCount = 100;
	Employee[] empArray = new Employee[2];
	
	public String getEmployee(int position) {
		return empArray[position].toString();
	}
	public boolean addEmp (Employee e) {
		boolean full = false;
		if (noOfEmployees < empArray.length) {
			empArray[noOfEmployees] = e;
			noOfEmployees++;
		}
		else {
			full = true;
		}
		return full;	
	}
}
